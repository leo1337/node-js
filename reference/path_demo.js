const path = require('path');

// base file name
console.log(path.basename(__filename));

//Directory name
console.log(path.dirname(__filename));

//File extension
console.log(path.extname((__filename)));

//  make object from  the all  above stuff
console.log(path.parse(__filename).base);

//console.log(path.parse(__filename));

// concatenate paths  C:\Users\leoha\WebstormProjects\node-js\reference --> C:\Users\leoha\WebstormProjects\node-js\reference\test\hello.html
console.log(path.join(__dirname,'test','hello.html'))