// file system ;
const fs = require('fs')
const path = require('path')

// Create folder
fs.mkdir(path.join(__dirname,'/test'), {},function (err){
    // throw shows error and stops executing
    if(err) throw err;
    console.log('folder created...');

});

// create and write to file second parameter is a content ,, it will override the file
fs.writeFile(
    path.join(__dirname,'/test','hello.txt'), 'hello world!!',function (err){
    // throw shows error and stops executing
    if(err) throw err;
    console.log('file written to ...');

// no override,  just append
        fs.appendFile(
            path.join(__dirname,'/test','hello.txt'), ' I love node.js',function (err){
                // throw shows error and stops executing
                if(err) throw err;
                console.log('file written to ...');

            });
});

//Read file ,,, options is encoding and callback needs data parameter
fs.readFile(path.join(__dirname,'/test','hello.txt'), 'utf8',function (err,data) {
    // throw shows error and stops executing
    if (err) throw err;
    console.log(data);
});

//Rename file
fs.rename(path.join(__dirname,'/test','hello.txt'), path.join(__dirname,'/test','helloworld.txt'),function (err) {
    // throw shows error and stops executing
    if (err) throw err;
    console.log('file has been renamed');
});