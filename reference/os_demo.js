// gives us information about environment and operating system
const os = require('os');

// shows our os platform for example : win32, darwin, linux
console.log(os.platform());

// CPU Arch
console.log(os.arch());

//CPU core info
console.log(os.cpus());

// Free memory
console.log(os.freemem());

// Total memory
console.log(os.totalmem());

//shows our Home dir
console.log(os.homedir());

//shows our system uptime number is seconds
console.log(os.uptime());

